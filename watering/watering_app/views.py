from . import models, forms, serializers
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import gettext_lazy as _
from django.views.decorators.http import require_GET, require_POST
from rest_framework import generics, permissions
from rest_framework.response import Response


@login_required
@require_GET
def frontpage_view(request):
    ctx = {}
    return render(request, "frontpage.html", ctx)


# Stations
##########


@login_required
@require_GET
def stations_view(request):
    stations = models.ClientStation.objects.all()
    ctx = {}
    ctx['stations'] = stations
    return render(request, "stations.html", ctx)


@login_required
@require_GET
def station_view(request, pk):
    obj = get_object_or_404(models.ClientStation, pk=pk)
    lst = models.TemperatureAndHumidity.objects.filter(client_station=pk).order_by('-id')[:24]
    obj_list = [
        {
            'created_at': o.created_at,
            'temperature': o.temperature + obj.temperature_correction,
            'humidity': o.humidity + obj.humidity_correction,
        }
        for o in lst
    ]
    ctx = {}
    ctx['station'] = obj
    ctx['obj_list'] = obj_list
    return render(request, "station.html", ctx)


@login_required
@require_GET
def station_edit_view(request, pk):
    if pk != 0:
        i = get_object_or_404(models.ClientStation, pk=pk)
        form = forms.ClientStationForm(instance=i)
    else:
        form = forms.ClientStationForm

    ctx = {}
    ctx['form'] = form
    ctx['pk'] = pk
    return render(request, "station_edit.html", ctx)


@login_required
@require_POST
def station_save_view(request):
    pk = int(request.POST['pk'])
    if pk == 0:
        form = forms.ClientStationForm(request.POST)
    else:
        instance = get_object_or_404(models.ClientStation, pk=pk)
        form = forms.ClientStationForm(request.POST, instance=instance)
    if form.is_valid:
        obj = form.save()
        pk = obj.pk
    else:
        for error in form.errors:
            messages.error(request, f"{'Form validation error'}: {error}")
    return redirect(station_view, pk=pk)


@login_required
@require_GET
def station_delete_view(request, pk):
    obj = get_object_or_404(models.ClientStation, pk=pk)
    obj.delete()
    return redirect(stations_view)


# Plants
########


@login_required
@require_GET
def plants_view(request):
    plants = models.Plant.objects.all().order_by('item_order')
    ctx = {}
    ctx['plants'] = plants
    return render(request, "plants.html", ctx)


@login_required
@require_GET
def plant_view(request, pk):
    obj = get_object_or_404(models.Plant, pk=pk)
    ctx = {}
    ctx['obj'] = obj
    ctx['queue'] = obj.watering_queue[:24]
    return render(request, "plant.html", ctx)


@login_required
@require_GET
def plant_edit_view(request, pk):
    if pk != 0:
        i = get_object_or_404(models.Plant, pk=pk)
        form = forms.PlantForm(instance=i)
    else:
        form = forms.PlantForm

    ctx = {}
    ctx['form'] = form
    ctx['pk'] = pk
    return render(request, "plant_edit.html", ctx)


@login_required
@require_POST
def plant_save_view(request):
    pk = int(request.POST['pk'])
    if pk == 0:
        form = forms.PlantForm(request.POST)
    else:
        instance = get_object_or_404(models.Plant, pk=pk)
        form = forms.PlantForm(request.POST, instance=instance)
    if form.is_valid:
        obj = form.save()
    else:
        for error in form.errors:
            messages.error(request, f"{'Form validation error'}: {error}")
    return redirect(plant_view, pk=obj.pk)


@login_required
@require_GET
def plant_delete_view(request, pk):
    obj = get_object_or_404(models.Plant, pk=pk)
    obj.delete()
    return redirect(plants_view)


@login_required
@require_GET
def plant_water_view(request, pk):
    obj = get_object_or_404(models.Plant, pk=pk)
    topping = obj.topping_in_ml if obj.topping_in_ml < obj.bottle_content else obj.bottle_content
    print(obj.topping_in_ml)
    print(obj.bottle_content)
    if topping > 0:
        topping_in_sec = int(topping * (1 + obj.topping_correction / 1000)) / 5
        models.WateringQueue.objects.create(
            plant=obj,
            type=models.Plant.TYPE_WATER,
            topping_in_ml=topping,
            topping_in_seconds=topping_in_sec,
            status=0,
            user=request.user,
        )
    return redirect(plant_view, pk=pk)


@login_required
@require_GET
def plant_refill_view(request, pk):
    obj = get_object_or_404(models.Plant, pk=pk)
    models.WateringQueue.objects.create(
        plant=obj,
        type=models.Plant.TYPE_REFILL,
        topping_in_ml=obj.bottle_capacity_in_ml,
        topping_in_seconds=0,
        status=1,
        user=request.user,
    )
    return redirect(plant_view, pk=pk)


# Login
#######
@require_GET
def user_login_view(request):
    ctx = {}
    return render(request, "login.html", ctx)


@require_POST
def user_login(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
    else:
        messages.error(request, _("Wrong username or password"))
    return redirect(frontpage_view)


@require_GET
def user_logout(request):
    logout(request)
    return redirect(frontpage_view)


# Api
#######


class WateringQueueListView(generics.ListAPIView):
    serializer_class = serializers.WateringQueueSerializer
    http_method_names = ['get']
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        pk = self.kwargs.get('pk')
        queryset = models.WateringQueue.objects.filter(
            plant__client_station=pk, status=0, type=models.Plant.TYPE_WATER
        )
        return queryset


class UpdateStatusView(generics.UpdateAPIView):
    queryset = models.WateringQueue.objects.all()
    serializer_class = serializers.WateringQueueSerializer
    http_method_names = ['patch']
    permission_classes = [permissions.IsAuthenticated]
    lookup_field = 'pk'

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()
            return Response({"message": "status updated successfully"})

        else:
            return Response({"message": "failed", "details": serializer.errors})


class TemperatureCreateView(generics.CreateAPIView):
    queryset = models.TemperatureAndHumidity.objects.all()
    serializer_class = serializers.TemperatureSerializer
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ['post']
