from django import forms
from . import models


class ClientStationForm(forms.ModelForm):
    class Meta:
        model = models.ClientStation
        fields = ("name", "temperature_correction", "humidity_correction")


class PlantForm(forms.ModelForm):
    class Meta:
        model = models.Plant
        fields = (
            "name",
            "item_order",
            "bottle_capacity_in_ml",
            "topping_in_ml",
            "topping_correction",
            "client_station",
            "gpio_pin",
        )
