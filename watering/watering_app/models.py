from django.contrib.auth.models import User
from django.db import models
from django.db.models import Sum
from django.utils.translation import gettext_lazy as _


class ClientStation(models.Model):
    name = models.CharField(_("Name"), max_length=50)
    temperature_correction = models.IntegerField(_("Temperature correction"), default=0)
    humidity_correction = models.IntegerField(_("Humidity correction"), default=0)

    def __str__(self):
        return f"ClientStation: {self.name}"

    class Meta:
        ordering = ['id']


class Plant(models.Model):
    item_order = models.IntegerField(_("Item order"))
    name = models.CharField(_("Name"), max_length=50)
    bottle_capacity_in_ml = models.IntegerField(_("Bottle capacity in ml"), default=0)
    topping_in_ml = models.IntegerField(_("Topping in ml"))
    topping_correction = models.IntegerField(_("Topping correction"), default=0)
    client_station = models.ForeignKey(ClientStation, on_delete=models.CASCADE)
    gpio_pin = models.IntegerField(_("GPIO pin"))

    TYPE_REFILL = 'refill'
    TYPE_WATER = 'water'

    @property
    def watering_queue(self):
        return self.wateringqueue_set.all()

    @property
    def bottle_content(self):
        last_refill = self.watering_queue.filter(type=self.TYPE_REFILL).order_by('-id')[:1]
        if last_refill.count() == 0:
            result = 0
        else:
            consumed = self.watering_queue.filter(type=self.TYPE_WATER, id__gte=last_refill[0].id).aggregate(
                Sum('topping_in_ml')
            )
            result = (
                last_refill[0].topping_in_ml - consumed['topping_in_ml__sum']
                if consumed['topping_in_ml__sum'] is not None
                else last_refill[0].topping_in_ml
            )
        return result

    def __str__(self):
        return f"Plant: {self.name}"

    class Meta:
        ordering = ['id']


class WateringQueue(models.Model):
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE)
    created_at = models.DateTimeField(_("Created"), auto_now_add=True)
    type = models.CharField(_("Type"), max_length=10)
    topping_in_ml = models.IntegerField(_("Topping in ml"))
    status = models.SmallIntegerField(null=False)
    topping_in_seconds = models.IntegerField(_("Topping in s"))
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    class Meta:
        ordering = ['-id']


class TemperatureAndHumidity(models.Model):
    client_station = models.ForeignKey(ClientStation, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    temperature = models.FloatField(_("Temperature"))
    humidity = models.FloatField(_("Humidity"))

    class Meta:
        ordering = ['-id']
