from . import views
from django.urls import path


urlpatterns = [
    path('', views.frontpage_view, name='frontpage'),
    path('stations/', views.stations_view, name='stations'),
    path('station/<int:pk>/', views.station_view, name='station'),
    path('station_edit/<int:pk>/', views.station_edit_view, name='station_edit'),
    path('station_save/', views.station_save_view, name='station_save'),
    path('station_delete/<int:pk>/', views.station_delete_view, name='station_delete'),
    path('plants/', views.plants_view, name='plants'),
    path('plant/<int:pk>/', views.plant_view, name='plant'),
    path('plant_edit/<int:pk>/', views.plant_edit_view, name='plant_edit'),
    path('plant_save/', views.plant_save_view, name='plant_save'),
    path('plant_delete/<int:pk>/', views.plant_delete_view, name='plant_delete'),
    path('plant_water/<int:pk>/', views.plant_water_view, name='plant_water'),
    path('plant_refill/<int:pk>/', views.plant_refill_view, name='plant_refill'),
    path('user_login/', views.user_login_view, name='user_login'),
    path('user_login_post/', views.user_login, name='user_login_post'),
    path('user_logout/', views.user_logout, name='user_logout'),
    path('api/watering_queue/<int:pk>/', views.WateringQueueListView.as_view(), name='api_watering_queue'),
    path('api/watering_queue/update/<int:pk>/', views.UpdateStatusView.as_view(), name='api_watering_queue_update'),
    path('api/temperature/create/', views.TemperatureCreateView.as_view(), name='api_temperature'),
]
