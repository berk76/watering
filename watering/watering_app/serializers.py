from rest_framework import serializers
from . import models


class WateringQueueSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.WateringQueue
        fields = ('id', 'plant', 'created_at', 'type', 'topping_in_ml', 'topping_in_seconds', 'status')
        depth = 1


class TemperatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TemperatureAndHumidity
        fields = ('client_station', 'temperature', 'humidity')
