from django.apps import AppConfig


class WateringAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'watering_app'
