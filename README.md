# Watering of plants

This project contains software part of my watering system based on RaspberryPi2 and [m5stack watering module](https://shop.m5stack.com/products/watering-unit-with-mositure-sensor-and-pump).

## How to run it

You will need python 3.10 or newer.

Clone repository
```
git clone https://codeberg.org/berk76/watering.git
```

Create virtual environment
```
cd watering
python3 -m venv venv
source venv/bin/activate
```

Install dependencies
```
pip install -r requirements
```

Install migrations
```
cd watering
python manage.py migrate
```

Create user
```
python manage.py createsuperuser
```

Compile messages
```
python manage.py compilemessages
```

Run app
```
python manage.py runserver
```

## How to run watering task

Schedule following task which is reading watering queue and runs watering of your plants.
```
python manage.py watering
```
