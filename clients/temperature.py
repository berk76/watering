import adafruit_dht
import board
import argparse
import requests
from requests.auth import HTTPBasicAuth
import json
from retry import retry


@retry(tries=3, delay=2)
def upload_data(server, client_no, user, password):
    dht_device = adafruit_dht.DHT22(board.D17, use_pulseio=False)
    temperature = dht_device.temperature
    humidity = dht_device.humidity
    print(f"Temperature: {temperature}, Humidity:{humidity}")

    url = f"{server}/api/temperature/create/"
    data = {'client_station': client_no, 'temperature': temperature, 'humidity': humidity}
    headers = {'Content-type': 'application/json', 'Accept': '*/*'}
    r = requests.post(url, data=json.dumps(data), headers=headers, auth=HTTPBasicAuth(user, password))
    print(r.status_code)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Upload temperature and humidity")
    required = parser.add_argument_group("required arguments")

    required.add_argument("-s", "--server", type=str, help="Server host", required=True)
    required.add_argument("-c", "--client-number", type=str, help="Client number", required=True)
    required.add_argument("-u", "--user", type=str, help="Username", required=True)
    required.add_argument("-p", "--password", type=str, help="Password", required=True)
    args = parser.parse_args()

    upload_data(args.server, args.client_number, args.user, args.password)
